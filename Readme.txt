问答社区项目开发规范：

一、项目模块划分：
q_a_community-auth:权限模块（Rbac方式管理权限）
q_a_community-web:web主模块（后台业务实现和项目打包部署）	
q_a_community-ui:ui模块（前端界面和业务实现)
q_a_community-elSearch:搜索模块（ElasticSearch)
q_a_community-nosql:大数据算法
q_a_community-common:通用工具模块

二、项目配置文件规范：
配置文件写在每个子项目src/main/properties文件夹下面，统一为spring-模块名*.xml格式
其中spring.xml和spring-mvc.xml为通用配置文件，这两个文件整合部署的时候以主模块的配置文件为准（重要）

{
	status:200,
	content-Type:application/json;charset=UTF-8,
	datas:[],
	exception:
	exceptionMsg:
}
